mod params;

use image::ImageBuffer;
use noise::{NoiseFn, Seedable};
use structopt::StructOpt;

/// The maximum value of the generated noise to scale the grey value
/// (should be given from the generated values).
const MAX: f64 = 1.;
const PI_X2: f64 = std::f64::consts::PI * 2.;

fn main() -> std::io::Result<()> {
    let params = dbg!(params::Params::from_args());
    let mut img_buf = ImageBuffer::new(params.width, params.height);

    let perlin = noise::Perlin::new().set_seed(params.seed);
    let fbm = noise::Fbm::new().set_seed(params.seed);
    let constant = noise::Constant::new(params.detail);
    let noise_gen = noise::Blend::new(&perlin, &fbm, &constant);

    let (width, height) = (f64::from(params.width), f64::from(params.height));
    let ratio = width / height;
    let (chaos_x, chaos_y) = (params.chaos * ratio, params.chaos);

    for (x, y, pixel) in img_buf.enumerate_pixels_mut() {
        let s = f64::from(x) / width;
        let t = f64::from(y) / height;

        let value = noise_gen.get([
            chaos_x * (s * PI_X2).cos() / PI_X2,
            chaos_y * (t * PI_X2).cos() / PI_X2,
            chaos_x * (s * PI_X2).sin() / PI_X2,
            chaos_y * (t * PI_X2).sin() / PI_X2,
        ]);

        let grey = (value + MAX) * 127.;
        if grey < 0. || grey > 255. {
            println!(">>> Invalid grey range: {}", grey);
        }

        *pixel = color(grey as u8);
    }

    let filename = format!(
        "seed{}_chaos{}_detail{}.png",
        params.seed, params.chaos, params.detail
    );
    img_buf
        .save(&filename)
        .map(|_| println!("File '{}' written.", filename))
}

fn color(grey: u8) -> image::Rgb<u8> {
    /// Color interpolation.
    fn inter(rgb1: [u8; 3], rgb2: [u8; 3], ratio: f32) -> image::Rgb<u8> {
        fn inter(a: u8, b: u8, ratio: f32) -> u8 {
            if b > a {
                let diff = f32::from(b - a) * ratio;
                a + diff as u8
            } else {
                let diff = f32::from(a - b) * ratio;
                a - diff as u8
            }
        }

        image::Rgb([
            inter(rgb1[0], rgb2[0], ratio),
            inter(rgb1[1], rgb2[1], ratio),
            inter(rgb1[2], rgb2[2], ratio),
        ])
    }

    let levels = [
        (0, [0, 0, 108]),       // ocean
        (81, [0, 128, 128]),    // water
        (126, [0, 206, 209]),   // border
        (131, [255, 220, 100]), // sand
        (141, [34, 139, 34]),   // plain
        (181, [128, 40, 0]),    // mountain
        (236, [255, 240, 230]), // snowy
        (249, [255, 255, 255]), // snow
        (255, [255, 255, 255]), // snow
    ];

    levels
        .windows(2)
        .find_map(|s| {
            let (bottom, c1) = s[0];
            let (top, c2) = s[1];

            if grey < top {
                let c = inter(c1, c2, f32::from(grey - bottom) / f32::from(top - bottom));
                Some(c)
            } else {
                None
            }
        })
        .unwrap_or_else(|| image::Rgb([255, 255, 255]))
}
