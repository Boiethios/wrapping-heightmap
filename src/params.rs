use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "wrapping heightmap generator")]
pub struct Params {
    /// Sets the seed.
    #[structopt(long = "seed", default_value = "0")]
    pub seed: u32,

    /// Sets the chaos factor. 1 is none.
    #[structopt(long = "chaos", default_value = "2")]
    pub chaos: f64,

    /// Sets the chiseling of the borders, from 0 to 3.
    #[structopt(long = "detail", default_value = "1")]
    pub detail: f64,

    /// Width of the image
    #[structopt(name = "WIDTH")]
    pub width: u32,

    /// Height of the image
    #[structopt(name = "HEIGHT")]
    pub height: u32,
}
