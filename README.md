# Wrapping heightmap

This is a small project that shows how to generate a tileable heightmap
with the [noise crate](https://docs.rs/noise/).

```none
$ ./wrapping-heightmap
wrapping heightmap generator 0.1.0
Boiethios <felix-dev@daudre-vignier.fr>

USAGE:
    wrapping-heightmap [OPTIONS] <WIDTH> <HEIGHT>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
        --chaos <chaos>      Sets the chaos factor. 1 is none. [default: 2]
        --detail <detail>    Sets the chiseling of the borders, from 0 to 3. [default: 1]
        --seed <seed>        Sets the seed. [default: 0]

ARGS:
    <WIDTH>     Width of the image
    <HEIGHT>    Height of the image
```

## Example

This is generated with the command:

```none
$ cargo run --release -- 800 600 --seed 72 --chaos 2 --detail 1
File 'seed72_chaos2_detail1.png' written.
```

![A tileable heightmap with seed 72, chaos 2, detail 1](example/seed72_chaos2_detail1.png)

## Tip

This project must be compiled in release mode because the compilation + execution time is faster in release mode than in debug mode.
